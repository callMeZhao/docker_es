import time
from subprocess import Popen
from procs_call import paral_call

n = 5 # n is the number of sub processes

command = "docker run sieve"

start = time.time()

paral_call(n, command)
end = time.time()
print(end - start)