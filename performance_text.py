import time
from procs_call import paral_call, sequ_call

def seq_command_exe_n(command, n_list, time_list):
	for n in n_list:
		start = time.time()
		sequ_call(n, command)
		end = time.time()
		time_list.append(end - start)
		print('sequ_call', command, n)
	pass

def paral_command_exe_n(command, n_list, time_list):
	for n in n_list:
		start = time.time()
		paral_call(n, command)
		end = time.time()
		time_list.append(end - start)
		print('paral_call', command, n)
	pass


n_list = [1, 2, 4, 8]

# command = "./sieve"
# time_list = list()
# seq_command_exe_n(command, n_list, time_list)

# with open('seq_latency.txt', 'w') as filehandle:
#     for time in time_list:
#         filehandle.write('%s\n' % time)


# command = "docker run sieve"
# docker_time_list = list()
# seq_command_exe_n(command, n_list, docker_time_list)

# with open('docker_seq_latency.txt', 'w') as filehandle:
#     for time in docker_time_list:
#         filehandle.write('%s\n' % time)


# command = "./sieve"
# para_time_list = list()
# paral_command_exe_n(command, n_list, para_time_list)

# with open('paral_latency.txt', 'w') as filehandle:
#     for time in para_time_list:
#         filehandle.write('%s\n' % time)


command = "docker run sieve"
docekr_paral_time_list = list()
paral_command_exe_n(command, n_list, docekr_paral_time_list)

with open('docker_paral_latency.txt', 'w') as filehandle:
    for time in docekr_paral_time_list:
        filehandle.write('%s\n' % time)

#
# ##### read text script
# # define an empty list
# time_plot = []
#
# # open file and read the content in a list
# with open('seq_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'seq time plot', time_plot)
#
# # define an empty list
# time_plot2 = []
#
# # open file and read the content in a list
# with open('docker_seq_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'seq docker time plot', time_plot)
#
# # define an empty list
# time_plot3 = []
# # open file and read the content in a list
# with open('paral_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'paral time plot', time_plot)
#
# # define an empty list
# time_plot4 = []
# # open file and read the content in a list
# with open('docker_paral_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'paral docker time plot', time_plot)
#
