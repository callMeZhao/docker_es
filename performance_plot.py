import matplotlib.pyplot as plt
import time
from procs_call import paral_call, sequ_call

## this script is to measure the performance (latency) of each scenario

def drawgraph(x, y, color, txt, plot, figure):
	plot.plot(x, y, color,label = txt)
	plot.title("Docker Experiment")
	plot.ylabel("performance(seconds)")
	plot.ylim([0, None])
	plot.xlabel("number of procs")
	plot.legend()

	pass


n_list = [1, 2, 4, 8]

figure = plt.figure()

command = "docker run sieve"
time_list = list()
for n in n_list:
	start = time.time()
	sequ_call(n, command)
	end = time.time()
	time_list.append(end - start)
	print('sequ_call', command, n)

drawgraph(n_list, time_list, 'y^', 'sequ with Docker', plt, figure)

command = './sieve'
time_list = list()
for n in n_list:
	start = time.time()
	sequ_call(n, command)
	end = time.time()
	time_list.append(end - start)
	print('sequ_call', command, n)

drawgraph(n_list, time_list, 'g^', 'sequ without Docker', plt, figure)

command = "docker run sieve"
time_list = list()
for n in n_list:
	start = time.time()
	paral_call(n, command)
	end = time.time()
	time_list.append(end - start)
	print('paral_call', command, n)
drawgraph(n_list, time_list, 'r^', 'paral with Docker', plt, figure)

command = './sieve'
time_list = list()
for n in n_list:
	start = time.time()
	paral_call(n, command)
	end = time.time()
	time_list.append(end - start)
	print('paral_call', command, n)

drawgraph(n_list, time_list, 'b^', 'paral without Docker', plt, figure)

plt.show()
figure.savefig('plot.png')