import time
from procs_call import paral_call

n = 10 # n is the number of sub processes

command = './sieve'

start = time.time()

paral_call(n, command)

end = time.time()

print(end - start)