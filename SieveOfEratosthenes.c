#include<stdio.h>
#include<time.h>

#include<sys/types.h>
#include<unistd.h>

int SieveOfEra (int);
int W2File (int, clock_t, clock_t, clock_t);

int main()
{	
	clock_t start_t = 0, end_t = 0, total_t;
	int n = 10000; // the number of prime numbers required
	

	pid_t pid =getpid();
	// printf("pid is: %d\n", pid); 

	start_t = clock();
	// printf("start at: %ld\n", start_t); 

	SieveOfEra(n);

	end_t = clock();
	// printf("end at: %ld\n", end_t);

	total_t = end_t - start_t;
	// printf("total is: %ld\n", total_t);

	// W2File(pid, start_t, end_t, total_t); // I/O write to text file

	return 0;
}

int SieveOfEra (int n){
	int i = 3, count, c;
	if(n >= 1){
			// printf("First %d prime numbers are: \n", n);
			// printf("2\n");
		}

	for (count = 2; count <= n; )
	{
			/* code */
		for (c =2; c <= i - 1; c++)
		{
			if(i%c == 0)
				break;
		}

		if (c == i)
		{
			// printf("%d\n", i);
			count++;
		}
		i++;
	}
	return 0;
}

int W2File(int pid, clock_t start, clock_t end, clock_t total){
	FILE *fp;
	fp = fopen("test.txt", "a+"); // open a file for both reading and writing. the reading start from the beginning but writing can only be appended
	fprintf(fp, "pid: %d, start: %ld, end: %ld, latency: %ld \n", pid, start, end, total);
	// fprintf(fp, "%d\n", pid);
	fclose(fp);
}