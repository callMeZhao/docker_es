.. (C) Ziyuan Zhao: copy & use
.. docker_es_doc documentation master file, created by
   sphinx-quickstart on Thu Aug 29 17:41:34 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to docker_es_doc's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

##########################
Docker on Embedded Devices
##########################

This document describes an experiment about the performance of docker used on embedded devices.

* :In the chapter Research Questions, it introduce the background and the topic of this experiment. 
* :In chapter Setting Up & Configuration, it introduce the required hardware as well as software configuration for. 
* :In chapter Result, it describe the output of the experiment and discuss the result. 
* :In chapter Evaluation, it assesses the settings of the experiment looks into the improvement as well as extensions.

`Chapter I: Research Questions`
####################################

`Chapter II: Setting Up & Configuration`
#############################################
The linux desktop used in this experiment has two cores and its CPUs works at 2594MHz. The embedded device (raspberry pi) used in this experiment has four cores and its CPUs works at 1200MHz(max) or 600MHz(min).

The application used in the test is sieve of eratosthenes. It is written in C, and is compiled to executables for the experimental platforms respectively. The execution time of an application (a unit) depends on the running platform and the parameter given by users, which is adjustable.

The docker container in the experiment is built from scratch.

Python script is used to invoke applications sequentially or simultaneously, and to measure the total execution time that is from the starting of the first application till the finish of the last one.

`Chapter III: Result`
##########################
The latency of executing one, two, four and eight applications on two platforms are measured and plotted. 

On the linux desktop, the latency of sequential executing containers is around 2 times of the concurrent one because there are two cores in the linux desktop. On the raspi, the latency of sequential containers execution is around 4 times of the concurrent one because there are four cores in the raspi.

On the linux desktop, there is an significant difference on latency between execution with and without docker. One is nearly becoming the double of the other one with the scale up. However, on the raspi, although execution with docker is slower than it without docker, the difference is around 10% of the longer one.

.. figure:: desktop_plot.png

.. figure:: pi_plot.png


 

`Chapter IV: Evaluation`
#############################
The number of applications should be scaled up, which means more experiment are needed with such different settings as different unit execution time, higher quantities.
