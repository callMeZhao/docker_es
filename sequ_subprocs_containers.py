import time
from proc_call import sequ_call

proc = 10 #n is number of subprocess
command = "docker run sieve"

start = time.time()
sequ_call(proc, command)
end = time.time()

print (end - start)