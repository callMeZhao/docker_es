import time
from procs_call import sequ_call

n = 10 #n is number of subprocess
command = "./sieve"

start = time.time()
sequ_call(n, command)
end = time.time()

print (end - start)